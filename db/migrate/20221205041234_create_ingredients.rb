class CreateIngredients < ActiveRecord::Migration[7.0]
  def change
    create_table :ingredients do |t|
      t.string :name
      t.decimal :number_of
      t.string :unit_of_measure

      t.timestamps
    end
  end
end
